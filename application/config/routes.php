<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['prijava'] = 'user/index';
$route['user/do_login'] = 'user/do_login';
$route['user/logout'] = 'user/logout';

$route['admin/(:any)'] = 'admin/$1';
$route['admin/(:any)/(:any)'] = 'admin/$1/$2';

$route['cms/(:any)/(:any)'] = '$1/$2';
$route['cms/(:any)/(:any)/(:any)'] = '$1/$2/$3';



$route['default_controller'] = "home";
$route['404_override'] = '';
$route['admin'] = 'admin/index';

$route['resize/make_thumb/(:any)'] = 'resize/make_thumb/$1';

$route['kontakt'] = 'contact/index';
$route['o-nama'] = 'subcategories/index';
$route['kolektiv'] = 'categories/collective';

// $route['bilingvalna-nastava/engleska/12'] = 'subcategories/england';
// $route['bilingvalna-nastava/francuska/11'] = 'subcategories/france';
$route['skola/galerija/21'] = 'subcategories/school';

$route['user/cir'] = 'user/cir';
$route['user/lat'] = 'user/lat';

$route['vesti'] = 'news/index';
$route['vesti/all'] = 'news/all';
$route['vesti/(:any)/(:num)'] = 'news/review/$3';

$route['aktivnosti'] = 'activities/index';
$route['aktivnosti/all'] = 'activities/all';
$route['aktivnosti/(:any)/(:num)'] = 'activities/review/$3';

$route['(:any)/(:any)/(:any)/(:num)'] = 'sorts/review/$4';


$route['(:any)/(:any)/(:num)/(:any)'] = 'subcategories/review/$3';
$route['(:any)/(:any)/(:num)'] = 'subcategories/review/$3';

$route['(:any)'] = 'categories/review/$1';






/* End of file routes.php */
/* Location: ./application/config/routes.php */