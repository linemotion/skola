<aside class="sidebar col">


<?php if ($this->uri->segment(2) == "akcija"): ?>
        <a href="/proizvodi/akcija" class="discounted disc-selected">Akcijski artikli</a>
<?php else: ?>
        <a href="/proizvodi/akcija" class="discounted">Akcijski artikli</a> 
<?php endif ?>

    <nav class="aside-nav">

        <ul>
    <?php foreach (modules::run("categories/get","position") as $category): ?>
        <?php if ($category->url == $this->uri->segment(2)): ?>
            <li class="aside-selected"><a href="/proizvodi/<?php echo $category->url;?>"><?php echo $category->name?></a>
                <ul>
                    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                        <?php if ($subcategory->url == $this->uri->segment(3)): ?>
                            <li class="aside-sub-selected"><a href="/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->name?></a></li>                        
                        <?php else: ?>
                            <li><a href="/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->name?></a></li>
                        <?php endif ?>
                    <?php endforeach ?> 
                </ul>
            </li>
        <?php else: ?>
                <li><a href="/proizvodi/<?php echo $category->url;?>"><?php echo $category->name?></a></li>
        <?php endif ?>
    <?php endforeach ?> 
        </ul>

    </nav>

    <h3 class="subtitle">
        Pogledajte i
    </h3>

    <div class="side-content">
        <a href="#" class="see">
            <img src="/assets/img/brw-kol.jpg" alt="">
        </a>
    </div>

</aside>
