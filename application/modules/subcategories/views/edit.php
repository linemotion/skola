         <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                        <ul class="sidebar-nav-groups">
                            <li>
                                <a href="/company/admin">Profil</a>
                            </li>
                            <li>
                                <a href="/management/admin">Menadžment</a>
                            </li>
                            <li  class="sidebar-nav-groups-selected">
                                <a href="/career/admin">Karijera</a>
                            </li>
                            <li>
                                <a href="/brands/admin">Brendovi</a>
                            </li>
                        </ul>                    
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>            </ul>
            </nav>
            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title"><?php echo $subcategory->name ?></h2>
                </div>

                <header class="tab-header group">
                    <a href="/cms/subcategories/admin" class="tab-1-2 tab-active">Vrste</a>
                    <a href="/cms/subcategories/content/<?php echo $this->uri->segment(4) ?>" class="tab-1-2">Sadržaj</a>
                </header>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">
                            Lista Vrsti
                            <a href="#" class="btn-add md-trigger" data-modal="modal-add-position"><i class="fa fa-plus-circle"></i> Dodaj vrstu</a>
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Naziv</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
<?php foreach ($sorts as $sort): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/cms/sorts/edit/<?php echo $sort->id; ?>"></i><?php echo $sort->name ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/cms/sorts/edit/<?php echo $sort->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                    <a href="#" class="act-btn del-btn md-trigger" data-modal="modal-del-position" data-id="<?php echo $sort->id ?>" data-controller="sorts"><i class="fa fa-times"></i>Obriši</a>
                                </td>
                            </tr>
<?php endforeach ?>
                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-add-position">
            <div class="md-content">
                <h3>Dodaj vrstu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime vrste</h4>

                        <form action="/cms/sorts/create/<?php echo $this->uri->segment(4) ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-position">
            <div class="md-content md-subcat-del">
                <h3>Obriši Podkategoriju <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu poziciju?</h4>

                        <form id="delete_cat_form">
                            <div class="form-bottom">
                                <button type="submit" class="btn-del-large"><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
       



















