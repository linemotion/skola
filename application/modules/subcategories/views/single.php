        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="inside-sidebar">
                        <ul class="inside-cats">
<?php if ($this->uri->segment(1) == "bilingvalna-nastava"): ?>
    <?php if ($this->session->userdata("lang") == "lat"): ?>
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $this->uri->segment(3)): ?>
                            <li class="ic-selected">
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo cirlat_front($item->name); ?></span>
                                </a>
                    <?php if (modules::run("sorts/get_where_custom", "subcat_id",$this->uri->segment(3) )): ?>
                                 <ul>
                        <?php foreach (modules::run("sorts/get_where_custom", "subcat_id",$item->id ) as $sort): ?>
                                    <li><a href="/<?php echo $sort->cat_name."/".$sort->subcat_name."/".$sort->url."/".$sort->id ?>"><?php echo cirlat_front($sort->name); ?></a></li>
                        <?php endforeach ?>
                                </ul>                       
                    <?php endif ?>
                            </li>
                    <?php else: ?>
                            <li>
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo cirlat_front($item->name); ?></span>
                                </a>
                            </li>
                    <?php endif ?>

         
                <?php endforeach ?>
    <?php else: ?>
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $this->uri->segment(3)): ?>
                            <li class="ic-selected">
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                    <?php if (modules::run("sorts/get_where_custom", "subcat_id",$this->uri->segment(3) )): ?>
                                 <ul>
                        <?php foreach (modules::run("sorts/get_where_custom", "subcat_id",$item->id ) as $sort): ?>
                                    <li><a href="/<?php echo $sort->cat_name."/".$sort->subcat_name."/".$sort->url."/".$sort->id ?>"><?php echo $sort->name; ?></a></li>
                        <?php endforeach ?>
                                </ul>                       
                    <?php endif ?>
                            </li>
                    <?php else: ?>
                            <li>
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                            </li>
                    <?php endif ?>

         
                <?php endforeach ?>
    <?php endif ?>    
<?php else: ?>
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $this->uri->segment(3)): ?>
                            <li class="ic-selected">
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                    <?php if (modules::run("sorts/get_where_custom", "subcat_id",$this->uri->segment(3) )): ?>
                                 <ul>
                        <?php foreach (modules::run("sorts/get_where_custom", "subcat_id",$item->id ) as $sort): ?>
                                    <li><a href="/<?php echo $sort->cat_name."/".$sort->subcat_name."/".$sort->url."/".$sort->id ?>"><?php echo $sort->name; ?></a></li>
                        <?php endforeach ?>
                                </ul>                       
                    <?php endif ?>
                            </li>
                    <?php else: ?>
                            <li>
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                            </li>
                    <?php endif ?>


                            
                <?php endforeach ?>

<?php endif ?>

            </ul>
                    </div> <!-- .inside-sidebar -->
                    <h5 class="feat-links-aside-title">Корисни линкови:</h5>
                    <ul class="feat-links-aside">
                    <?php foreach ($useful as $links): ?>
                        <li>
                            <a href="/<?php echo url_title(rs_char($links->cat_name)) ?>/<?php echo $links->url ?>/<?php echo $links->id ?>">
                                <i class="icon-<?php echo $links->icon; ?>"></i>
                                <span><?php echo $links->name; ?></span>
                            </a>
                        </li>
                    <?php endforeach ?>                    </ul>
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <article class="inside-article">
<?php if ($this->uri->segment(1) == "bilingvalna-nastava"): ?>
                       
<?php if ($this->session->userdata("lang") == "lat"): ?>
                             <h1><?php echo cirlat_front($subcategory->name) ?>
                            <a href="/user/cir" class="lang-switch">Ћирилица</a></h1>
                            <?php echo cirlat_front($subcategory->desc);?>  
<?php else: ?>
                             <h1><?php echo $subcategory->name ?>
                            <a href="/user/lat" class="lang-switch">Latinica</a></h1>
                            <?php echo $subcategory->desc;?>  
<?php endif ?>  
<?php else: ?>
                        <h1><?php echo $subcategory->name ?></h1>
                        <?php echo $subcategory->desc;?>                          
<?php endif ?>       
                    </article> <!-- .inside-article -->
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->