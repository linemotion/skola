<?php

class Subcategories extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_subcategories');
		$this->module = "subcategories";

		if (!$this->session->userdata("lang"))
		{
			$sess_data['lang'] = "cir";
			$this->session->set_userdata($sess_data);
		}


	}
	function get_num_by_paginateSearch($search) 
	{
		$num = $this->mdl_subcategories->get_num_search($search);
		return $num;
	}
	function get_where_paginateSearch($search,$per_page,$offset) 
	{
		$subcategories = $this->mdl_subcategories->get_where_paginateSearch($search,$per_page,$offset);

		return $subcategories;
	}
	function search()
	{
		$data['search'] = $this->uri->segment(3);
		
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/proizvodi"),
			array("name" => "Pretraga","link" => ""),
			array("name" => $data['search'],"link" => "")
			); 	
		//$data['products'] = $this->get_by_search($search);
		$this->load->view('search',$data);

	}
	function index()
	{
		redirect("/o-nama/istorijat/1");		
	}
	function england()
	{
		redirect("/bilingvalna-nastava/engleska/informacije-za-prijemni-ispit/4");		
	}
	function france()
	{
		redirect("/bilingvalna-nastava/francuska/informacije-za-prijemni-ispit/1");		
	}
	function school()
	{
		redirect("/skola/galerija/prostorije-skole/7");		
	}
	function review()
	{
		$url = $this->uri->segment(3);
		
			$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(1));
			$data['subcategory'] =modules::run("subcategories/get_by_id",$this->uri->segment(3));

			//uzas jebeni
			// $this->db->where('id',$url);
			// $query = $this->db->get("subcategories");
			//kraj uzasa
	    	
	    	$data['useful'] = modules::run('subcategories/get_with_limit',3,0,"id");
		    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
		    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);


			//$data['subcategory'] = $query->row();
			$data['subcategories'] =modules::run("subcategories/get_where_custom","cat_id",$data['subcategory']->cat_id);
			
			$data['breadcrumbs'] = array(
			array("name" => $data['category']->name,"link" => "/".$data['category']->url),
			array("name" => $data['subcategory']->name,"link" => "/")
				); 		

		echo modules::run('template/render',$this->module,"single",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$subcategories = modules::run("subcategories/get","position");

		foreach ($subcategories as $subcategory) 
		{
			$data['url'] = url_title(rs_char($subcategory->name));
			$data['image'] = url_title(rs_char($subcategory->name)).".jpg";

			$this->_update($subcategory->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}
	function create()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		if (!$this->uri->segment(4))
			$data['cat_id'] = $this->input->post('select');
		else
			$data['cat_id'] = $this->uri->segment(4);

		$data['name'] = $this->input->post('name');
		//$data['position'] = $this->get_max_pos()->position + 1;
		$data['cat_name'] = modules::run('categories/get_by_id',$data['cat_id'])->name;
		
		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);


		$this->_insert($data);

		redirect('/cms/categories/edit/'.$data['cat_id']);

	}
	function change()
	{

		$id = $this->input->post('id');


		$output = '<select name="subcategories" class="uniformselect" id="subgroup"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('cat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}
	function content()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/proizvodi")
			); 		
		$data['subcategory'] = modules::run("subcategories/get_by_id",$this->uri->segment(4));

		echo modules::run('template/admin_render',$this->module,"content",$data);
	}	
	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		$data['subcategory'] = $this->get_by_id($id);
		$data["sorts"] = modules::run("sorts/get_where_custom","subcat_id",$id);
					// $config['total_rows'] = count(modules::run("products/get_where_custom_admin",'subcat_id',1));

		// echo "<pre>";
		// print_r($config);
		// die();
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		$data['subcategory'] = $this->get_by_id($id);
		
		$this->_delete($id);
		redirect("cms/categories/edit/".$data["subcategory"]->cat_id);
	}
	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);
		$subcategory = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['desc'] = $this->input->post('desc');
		$data['cat_id'] = $subcategory->cat_id;
		$data['cat_name'] = modules::run('categories/get_by_id',$data['cat_id'])->name;
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$subcategory->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/cms/subcategories/content/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_subcategories->get_max_pos();
		return $max_pos;
	}

	function get($order_by)
	{
		$subcategories = $this->mdl_subcategories->get($order_by);
		return $subcategories;
	}

	function get_sidebar_subcat_id()
	{
		$module = $this->uri->segment(1);
		$id = $this->uri->segment(3);

		if ($this->uri->segment(2) == "create" AND $module == "products") 
			return $this->get_dropdown_subcat_id();

		switch ($module) {
			case 'subcategories':
				$subcat_id = $id;
				break;
			case 'products':
				$subcat_id = modules::run('products/get_by_id',$id)->subcat_id;
				break;							
			default:
				# code...
				break;
		}
		return $subcat_id;
	}

	function get_dropdown_subcat_id()
	{
		$module = $this->uri->segment(4);
		$id = $this->uri->segment(3);


		switch ($module) {
			case 'subcategories':
				$subcat_id = $id;
				break;
			case 'products':
				$subcat_id = modules::run('products/get_by_id',$id)->subcat_id;
				break;							
			default:
				# code...
				break;
		}
		return $subcat_id;
	}
	function get_with_limit($limit, $offset, $order_by) 
	{
		$subcategories = $this->mdl_subcategories->get_with_limit($limit, $offset, $order_by);
		return $subcategories;
	}

	function get_by_id($id)
	{
		$subcategories = $this->mdl_subcategories->get_where($id);
		return $subcategories;
	}

	function get_where_custom($col, $value) 
	{
		$subcategories = $this->mdl_subcategories->get_where_custom($col, $value);		
		return $subcategories;
	}

	function get_by_url($url) 
	{
		$subcategories = $this->mdl_subcategories->get_by_url($url);		
		return $subcategories;
	}

	function _insert($data)
	{
		$this->mdl_subcategories->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_subcategories->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_subcategories->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_subcategories->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_subcategories->get_max();
		return $max_id;
	}
	function catnames() 
	{
		$subs = $this->get("position");

		foreach ($subs as $sub)
		{
			$data['cat_name'] = modules::run('categories/get_by_id',$sub->cat_id)->name;
			$this->_update($sub->id,$data);
		}
	}
	function _custom_query($mysql_query) 
	{
		$subcategories = $this->mdl_subcategories->_custom_categories($mysql_query);
		return $subcategories;
	}

}