        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="inside-sidebar contact-sidebar">
                        <span><b>Десета Гимназија<br>„Михајло Пупин”</b></span>
                        <span><b>Адреса:</b><br>Антифашистичке Борбе 1а</span>
                        <span><b>Телефон:</b><br>011/3114-142</span>
                        <span><b>Жиро рачун:</b><br>840-967666-49</span>
                        <hr>
                        <span><b>Директор:</b><br>011/2135-722<br><a href="mailto:direktor@xgimnazija.edu.rs">direktor@xgimnazija.edu.rs</a></span>
                        <span><b>Секретар:</b><br>011/3114-142, 011/2144-740<br><a href="mailto:sekretar@xgimnazija.edu.rs">sekretar@xgimnazija.edu.rs</a></span>
                        <span><b>E-mail:</b><br><a href="mailto:info@xgimnazija.edu.rs">info@xgimnazija.edu.rs</a></span>
                        <span><b>Професорска зборница:</b><br>011/3130-661</span>

                    </div>
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <div class="map-outer">
                        <div id="map"></div>
                        <small>До школе се најлакше стиже градским аутобусима број: <b>68, 17, 74, 16, 83, 78, 65, 95 и 75.</b></small>
                    </div>
                    <form action="/contact/send" class="contact-form">
                        <div>
                            <label for="ime">Име:</label>
                            <input type="text" id="ime" name="name" placeholder="Унесите ваше име">
                        </div>
                        <div>
                            <label for="email">E-mail адреса:</label>
                            <input type="text" id="email" name="mail" placeholder="Унесите вашу e-mail адресу">
                        </div>
                        <div>
                            <label for="poruka">Порука:</label>
                            <textarea id="poruka" name="message" placeholder="Унесите поруку"></textarea>
                        </div>
                        <button type="submit" class="btn-default">Пошаљи <i>&rarr;</i></button>
                    </form>
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->
