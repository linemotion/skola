                    <section class="main col">

                        <div class="section-content">

                            <h3 class="section-title">
                                Kontakt
                            </h3>

                            <div class="contactgroup group">

                               <form class="contactform group">

                                    <p><strong>Hvala!</strong></p>
                                    <p>Vaša poruka je poslata.</p>

                                </form>

                                <div class="contactinfo">

                                    <h3>Beograd</h3>
                                    <p>Autoput 18 (BG-ZG), TC Zmaj
                                        (Market DIS PUS)</p>
                                    <p>
                                        tel: 011 65 56 334<br>
                                        tel: 011 65 56 335<br>
                                        <a href="mailto:beograd@vitorogpromet.rs">beograd@vitorogpromet.rs</a>
                                    </p>

                                    <h3>Novi Sad</h3>
                                    <p>Hajduk Veljkova 11, Hala Novosadskog sajma<br>
                                    </p>
                                    <p>
                                        pon-pet: 8-21h,
                                        subota: 8-18h,
                                        ned: 10-16h
                                    </p>
                                    <p>
                                        tel: 021 472 4872<br>
                                        tel: 021 427 433<br>
                                        <a href="mailto:office@vito.rs">office@vito.rs</a>
                                    </p>

                                    <h3>Subotica</h3>
                                    <p>Bajnatska BB, Bivsi TUS<br>
                                    </p>

                                    <p>
                                        pon-pet: 8-21h,
                                        subota: 8-18h
                                    </p>

                                    <p> 
                                        tel: 024 538 825<br>
                                        <a href="mailto:subotica@vitorogpromet.rs">subotica@vitorogpromet.rs</a>
                                    </p>


                                    <h3>Sremska Mitrovica</h3>
                                    <p>Trg Svetog Stefana 23, Bivši salon "Novi Dom"
                                    </p>

                                    <p>
                                        pon-pet: 8-21h,
                                        subota: 8-15h
                                    </p>

                                    <p>
                                        tel: 022 621 350<br>
                                        <a href="mailto:sremskamitrovica@vitorogpromet.rs">sremskamitrovica@vitorogpromet.rs</a>
                                    </p>

                                </div> <!-- .contactinfo -->

                            </div> <!-- .contactgroup -->

                            <a href="#" class="loc-link">
                                Naše lokacije &rarr;
                            </a>

                        </div> <!-- .section-content -->
                        
                    </section>