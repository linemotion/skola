<div class="centercontent">
    


        <div class="pageheader notab">
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
            
                                        
                    <div class="contenttitle2">
                        <h3>Nova grupa</h3>
                    </div><!--contenttitle-->
                                        
                    <form class="stdform" action="/gallery/create_group/" method="post" enctype="multipart/form-data">
                        
                        <p>
                            <label>Ime nove grupe:</label>
                            <span class="field"><input type="text" name="name" class="smallinput" /></span>
                            <small class="desc">Što kraće to bolje</small>
                        </p>
                                                
                        <p class="stdformbutton">
                            <button class="submit radius2">Pošalji</button>
                        </p>
                        
                        
                    </form>
                    

                    <div class="contenttitle2">
                        <h3>Lista grupa</h3>
                    </div><!--contenttitle-->
                    <div align="center">
                        
<?php foreach ($groups as $group): ?>
                <li><a href="/gallery/show/<?php echo $group->id; ?>"><?php echo $group->name; ?></a></li>
<?php endforeach ?>

                    </div>

                    <br clear="all" /><br />
                   
        
        </div><!--contentwrapper-->
        
    </div><!-- centercontent -->