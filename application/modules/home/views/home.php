<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Десета гимназија - Михајло Пупин</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/additional.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

        <header class="site-header">
            <div class="container group">
                <h1 class="site-logo">
                    <a href="/"><img src="img/xgimn-logo.png" alt="Десета гимназија - Михајло Пупин"></a>
                </h1>
                <nav class="site-nav">
                    <ul class="site-nav-small">
                        <li class="site-nav-active"><a href="/">Насловна</a></li>				      
                <?php foreach ($categories as $category): ?>
                        <li><a href="/<?php echo $category->url ?>"><?php echo $category->name; ?></a></li>
                <?php endforeach ?>
                    </ul>
                    <ul class="site-nav-main">
                <?php foreach ($featured_cat as $cat): ?>
                        <li><a href="/<?php echo $cat->url ?>"><?php echo $cat->name; ?></a></li>
                <?php endforeach ?>
                    </ul>
                </nav> <!-- .site-nav -->
            </div> <!-- .container -->
        </header> <!-- .site-header-->

        <section class="home-hero">
            <div class="home-hero-inside">
                <div class="container group">
                    <article class="home-latest-news">
                        <h3 class="hl-subtitle">Вести</h3>
                        <small class="hl-date"><?php echo $news->date; ?></small>
                        <h2 class="hl-title"><a href="/vesti/<?php echo url_title(rs_char(cirlat($news->name))); ?>/<?php echo $news->id; ?>"><?php echo $news->name; ?></a></h2>
                        <?php echo $news->short_desc; ?>
                        <a href="/vesti/<?php echo url_title(rs_char(cirlat($news->name))); ?>/<?php echo $news->id; ?>" class="btn-white">Више <i>&rarr;</i></a>
                    </article> <!-- .home-latest-news -->
                    <div class="home-bilingual">
                        <h2>Билингвална настава</h2>
                        <div class="home-flags">
                            <a href="/bilingvalna-nastava/francuska/11">
                                <img src="/img/fr.png">
                            </a>
                            <a href="/bilingvalna-nastava/engleska/12">
                                <img src="/img/en.png">
                            </a>
                        </div> <!-- .home-flags -->
                        <p class="bl-titles">
                            Bilingvalna nastava<br>
                            Bilingual class<br>
                            Classe bilingue
                        </p> <!-- .bl-titles -->
                        <a href="/bilingvalna-nastava" class="btn-white">Више <i>&rarr;</i></a>
                    </div> <!-- .home-bilingual -->
                </div> <!-- .container -->
            </div> <!-- .home-hero-inside -->
        </section> <!-- .home-hero -->

        <section class="home-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="activity-sidebar">
                        <h3>Школске активности</h3>
                        <ul>
                <?php foreach ($activities as $activity): ?>
                			<li>
                                <a href="/aktivnosti/<?php echo url_title(rs_char(cirlat($activity->name))); ?>/<?php echo $activity->id ?>">
                                    <small><?php echo $activity->date; ?></small>
                                    <span><?php echo $activity->name; ?> <i>&rarr;</i></span>
                                </a>
                            </li>
                <?php endforeach ?>                            
                        </ul>
                        <a href="/aktivnosti" class="btn-default">Све Активности <i>&rarr;</i></a>
                    </div> <!-- .projects-sidebar -->
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <article class="home-vision">
                        <h2>Школа отвореног ума</h2>
                        <p>Визија наше школе заснива се, пре свега, на обезбеђивању квалитетног наставног процеса који подразумева употребу савремених метода и средстава. Настојимо да највише пажње придајемо повезивању садржаја једног предмета са другим предметима и негујемо учење са примерима из свакодневног живота.</p>
                        <p>То омогућава ученицима развој логичког мишљења, неговање инвентивности и стицање функционалних знања која ће моћи да употребе у свакодневном животу. Наставници и ученици треба да функционишу као тим који почива на толеранцији и флексибилности, а чији је заједнички циљ постизање што бољих резултата.</p>
                    </article> <!-- .home-vision -->
                    <div class="feat-links-home">
                        <h3>Корисни линкови:</h3>
                        <div class="group">
                    	<?php foreach ($useful as $links): ?>
                            <div class="col-1-3">
                                <a href="/<?php echo url_title(rs_char($links->cat_name)) ?>/<?php echo $links->url ?>/<?php echo $links->id ?>">
                                    <i class="icon-<?php echo $links->icon; ?>"></i>
                                    <span><?php echo $links->name; ?></span>
                                </a>
                            </div>                    		
                    	<?php endforeach ?>
                        </div> <!-- .group -->
                    </div> <!-- .feat-links-home -->
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->

        <footer class="site-pre-footer">
            <div class="container">
                <nav class="footer-nav group">
                    <ul>
                        <li><a href="/">Насловна</a></li>
<?php foreach (modules::run("categories/get_footer_nosub") as $nosub): ?>
                        <li><a href="/<?php echo $nosub->url ?>"><?php echo $nosub->name; ?></a></li>                           
<?php endforeach ?>
                    </ul>
<?php foreach (modules::run("categories/get_footer_sub") as $sub): ?>
                    <ul>
                        <li><a href="/<?php echo $sub->url ?>"><?php echo $sub->name; ?></a></li>
<?php foreach (modules::run("subcategories/get_where_custom","cat_id",$sub->id) as $subcategory): ?>
                        <li><a href="/<?php echo $sub->url ?>/<?php echo $subcategory->url ?>/<?php echo $subcategory->id ?>"><?php echo $subcategory->name; ?></a></li>                                               
<?php endforeach ?>    
                    </ul>                 
<?php endforeach ?>
             </nav>
            </div>
        </footer> <!-- .site-pre-footer -->

        <footer class="site-footer">
            <div class="container group">
                <div class="footer-meta">
                    <p class="footer-meta-info">
                        Десета Гимназија – Антифашистичке борбе 1а, 011/2135-722<br>
                        <a href="mailto:kontakt@xgimnazija.edu.rs">kontakt@xgimnazija.edu.rs</a>
                    </p>
                    <p class="footer-social">
                        <a href="https://www.facebook.com/groups/1508541572761066/?fref=ts"><i class="icon-facebook"></i></a>
                        <a href="https://www.youtube.com/channel/UC9O1TnW3WenulgjxEyoD0fA"><i class="icon-youtube"></i></a>
                    </p>
                </div> <!-- .footer-meta -->
                <div class="footer-friends">
                    <a href="#"><img src="/img/institut.png" alt=""></a>
                    <a href="#"><img src="/img/ministarstvo.png" alt=""></a>
                </div> <!-- .footer-friends -->
            </div> <!-- .container -->
        </footer> <!-- .site-footer -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/vendor/enquire.min.js"></script>
        <script src="js/default.js"></script>

    </body>
</html>
