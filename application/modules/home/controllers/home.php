<?php

class Home extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		// $data['breadcrumbs'] = array();

	    $data['news'] = modules::run('news/get_home');
	    $data['activities'] = modules::run('activities/get_with_limit',6,0,"id");
	    $data['subcategories'] = modules::run('subcategories/get',"id");

	    $data['useful'] = modules::run('subcategories/get_with_limit',3,0,"id");
	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);

	    // var_dump($data);die();
	    // $data['gallery2'] = modules::run('news_gallery/get',"id");

	    //$data['products'] = modules::run('products/get_home');

		$this->load->view('home',$data);
	}
}