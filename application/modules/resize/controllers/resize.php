<?php

class Resize extends MX_Controller
{
	var $upload_path;
	var $image_path;

    function __construct()
    {
		parent::__construct();
		$this->load->library('image_moo');

		$this->upload_path = FCPATH.'temp'.DIRECTORY_SEPARATOR;
		$this->image_path = FCPATH.'images'.DIRECTORY_SEPARATOR;
    }
    public function index()
    {   

		$products = modules::run("products/get","id");	

		for ($i=0; $i < count($products); $i++) 
    	{ 		
    		$product = modules::run("products/get_by_id",$products[$i]->id);
	
	    	$file_path = $this->upload_path."products".DIRECTORY_SEPARATOR.$product->image;

	    	if (!file_exists($file_path)) 
	    	{
	    		//rename($file_path, $this->upload_path."products".DIRECTORY_SEPARATOR.url_title(rs_char($product->name)).".jpg");
				echo $product->cat_name."/".$product->subcat_name."/".$product->name."/".$product->id." - <br>";
	    	}
		}
		die("done");
	}
    public function upload($module, $submodule = "")
    {   	

		$this->image_moo->load($_FILES['userfile']['tmp_name'])
						->save($this->upload_path.DIRECTORY_SEPARATOR.$_FILES['userfile']['name'],TRUE);
    	$this->image_moo->clear();

    	$image_size = @getimagesize($_FILES['userfile']['tmp_name']);

		if($image_size[0] > 1600)
		{
			if ($submodule != "") 
				$this->image_moo->load($_FILES['userfile']['tmp_name'])
										->resize('1600','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$submodule."/".$_FILES['userfile']['name'],TRUE);
			else
				$this->image_moo->load($_FILES['userfile']['tmp_name'])
										->resize('1600','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$_FILES['userfile']['name'],TRUE);
		}
		else
		{
			if ($submodule != "") 
				$this->image_moo->load($_FILES['userfile']['tmp_name'])
										->resize($image_size[0],$image_size[1])
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$submodule."/".$_FILES['userfile']['name'],TRUE);
			else
				$this->image_moo->load($_FILES['userfile']['tmp_name'])
										->resize($image_size[0],$image_size[1])
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$_FILES['userfile']['name'],TRUE);
		}

		if ($module == "news" || $module == "activities" || $module == "sorts") 
			$this->thumbs($module,$submodule);

		return $_FILES['userfile']['name'];
	}
    public function delete($module, $file_name)
    {   	
    	if (file_exists($this->upload_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$file_name))
    		unlink($this->upload_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$file_name);
	}
	public function thumbs($module,$submodule = "")
	{
    	$image_size = @getimagesize($_FILES['userfile']['tmp_name']);

		if($image_size[0] < $image_size[1])
		{
			if ($submodule != "") 
			$this->image_moo->load($_FILES['userfile']['tmp_name'])
									->resize('200','9999')
									->resize_crop("200","200")
									->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$submodule."/thumbs/".$_FILES['userfile']['name'],TRUE);
			else
			$this->image_moo->load($_FILES['userfile']['tmp_name'])
									->resize('200','9999')
									->resize_crop("200","200")
									->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumbs/".$_FILES['userfile']['name'],TRUE);


		}
		else
		{
			if ($submodule != "") 
			$this->image_moo->load($_FILES['userfile']['tmp_name'])
									->resize('9999','200')
									->resize_crop("200","200")
									->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$submodule."/thumbs/".$_FILES['userfile']['name'],TRUE);
			else
			$this->image_moo->load($_FILES['userfile']['tmp_name'])
									->resize('9999','200')
									->resize_crop("200","200")
									->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumbs/".$_FILES['userfile']['name'],TRUE);
		}
	
	}

    public function make_thumb()
    {   	

    	$gall_group = $this->uri->segment(3);
		$this->load->helper('file');
		$files = get_filenames($_SERVER['DOCUMENT_ROOT']."/img/gallery/".$gall_group);
		//$dirandfiles = get_dir_file_info($_SERVER['DOCUMENT_ROOT']."/img/gallery");


		// echo "<pre>";
		// print_r($files);
		// print_r($dirandfiles);
		// die();

		foreach ($files as $file)
		{
	    	$image_size = @getimagesize($_SERVER['DOCUMENT_ROOT']."/img/gallery/".$gall_group."/".$file);

			if($image_size[0] < $image_size[1])
			{
				$this->image_moo->load($_SERVER['DOCUMENT_ROOT']."/img/gallery/".$gall_group."/".$file)
										->resize('200','9999')
										->resize_crop("200","200")
										->save($_SERVER['DOCUMENT_ROOT']."/img/gallery/".$gall_group."/thumbs/".$file,TRUE);
			}
			else
			{
				$this->image_moo->load($_SERVER['DOCUMENT_ROOT']."/img/gallery/".$gall_group."/".$file)
										->resize('9999','200')
										->resize_crop("200","200")
										->save($_SERVER['DOCUMENT_ROOT']."/img/gallery/".$gall_group."/thumbs/".$file,TRUE);
			}


		}
		// $this->image_moo->load($_FILES['userfile']['tmp_name'])
		// 				->save($this->upload_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$_FILES['userfile']['name'],TRUE);
  //   	$this->image_moo->clear();

  //   	$image_size = @getimagesize($_FILES['userfile']['tmp_name']);

		// if($image_size[0] > 600)
		// {
		// 	$this->image_moo->load($_FILES['userfile']['tmp_name'])
		// 							->resize('600','9999')
		// 							->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$_FILES['userfile']['name'],TRUE);
		// }
		// else
		// {
		// 	$this->image_moo->load($_FILES['userfile']['tmp_name'])
		// 							->resize($image_size[0],$image_size[1])
		// 							->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$_FILES['userfile']['name'],TRUE);
		// }
		die("done");

		// return $_FILES['userfile']['name'];
	}

		
}