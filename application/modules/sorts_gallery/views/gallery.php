<div class="centercontent">
    


        <div class="pageheader notab">
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
            
                                        
                    <div class="contenttitle2">
                        <h3>Nova Slika</h3>
                    </div><!--contenttitle-->
                                        
                    <form class="stdform" action="/gallery/create/<?php echo $this->uri->segment(3); ?>" method="post" enctype="multipart/form-data">
                        
                        <p>
                            <label>Slika proizvoda:</label>
                            <br>

                            <span class="field">
                                <input type="file" name="userfile" />
                            </span>
                            <small class="desc">RGB boje. Formati: JPEG, GIF, PNG</small>
                        </p>
                        <p class="stdformbutton">
                            <button class="submit radius2">Pošalji</button>
                        </p>
                    </form>
                    

                    <div class="contenttitle2">
                        <h3>Lista Slika</h3>
                    </div><!--contenttitle-->
                    <div align="center">
<?php if (count($images) == 1): ?>
                <img src="/img/gallery/<?php echo $images[0]->image; ?>" width="200" height="200">
                <a href="/gallery/delete/<?php echo $images[0]->id ?>/<?php echo $this->uri->segment(3); ?>">X</a><br>
                Jedna slika
<?php else: ?>
<?php foreach ($images as $item): ?>
                <img src="/img/gallery/<?php echo $item->image; ?>" width="200" height="200">
                <a href="/gallery/delete/<?php echo $item->id ?>/<?php echo $this->uri->segment(3); ?>">X</a><br>
<?php endforeach ?>  
                Galerija   
<?php endif ?>


                    </div>

                    <br clear="all" /><br />
                   
        
        </div><!--contentwrapper-->
        
    </div><!-- centercontent -->