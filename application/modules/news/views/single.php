        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="inside-sidebar">
                        <ul class="inside-cats">
                <?php foreach ($news as $item): ?>
                    <?php if ($item->id == $this->uri->segment(3)): ?>
                            <li class="ic-selected">
                    <?php else: ?>
                            <li>
                    <?php endif ?>
                                <a href="/vesti/<?php echo url_title(rs_char(cirlat($item->name))); ?>/<?php echo $item->id ?>">
                                    <small><?php echo $item->date ?></small>
                                    <span><?php echo $item->name ?></span>
                                </a>
                            </li>
                <?php endforeach ?>
                        </ul>
                        <a href="/vesti" class="btn-default btn-reverse"><i>&larr;</i> Све вести</a>
                    </div> <!-- .inside-sidebar -->
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <article class="inside-article">
                        <h1><?php echo $new->name ?> <span class="article-date"><i class="icon-date"></i> <?php echo $new->date ?></span></h1>
<?php if ($new->image): ?>
                           <a href="/img/news/<?php echo $new->image ?>" class="fancybox"><img src="/img/news/<?php echo $new->image ?>" class="img-top-left"></a>                         
                            <?php echo $new->desc ?>
<?php else: ?>
    <?php if ($gallery): ?>
                            <?php echo $new->desc ?>
                            <div class="gallery">
                                <ul class="gallery-list group">
    <?php foreach (modules::run("gallery/get_where_custom","news_id",$this->uri->segment(3)) as $image): ?>
                                    <li><a href="/img/news/<?php echo $image->image ?>" rel="gallery" class="fancybox"><img src="/img/news/thumbs/<?php echo $image->image ?>"></a></li>    
    <?php endforeach ?>
                                </ul>
                            </div> <!-- .gallery -->
                            
    <?php else: ?>        
                            <?php echo $new->desc ?>                    
    <?php endif ?>

<?php endif ?> 
                    </article> <!-- .inside-article -->
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->
