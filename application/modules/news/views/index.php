

        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar site-sidebar-b">
                    <h5 class="feat-links-aside-title">Корисни линкови:</h5>
                    <ul class="feat-links-aside">
                    <?php foreach ($useful as $links): ?>
                        <li>
                            <a href="/<?php echo url_title(rs_char(cirlat($links->cat_name))) ?>/<?php echo $links->url ?>/<?php echo $links->id ?>">
                                <i class="icon-<?php echo $links->icon; ?>"></i>
                                <span><?php echo $links->name; ?></span>
                            </a>
                        </li>
                    <?php endforeach ?> 
                    </ul>
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <h1 class="section-title">Све вести</h1>

                    <div class="all-news">
                    <?php foreach ($news as $new): ?>
                        <div class="all-news-item">
                            <h3><a href="/vesti/<?php echo url_title(rs_char(cirlat($new->name))) ?>/<?php echo $new->id ?>"><?php echo $new->name ?></a></h3>
                            <p><?php echo $new->short_desc;?></p>
                            <a href="/vesti/<?php echo url_title(rs_char(cirlat($new->name))) ?>/<?php echo $new->id ?>" class="btn-default">Више <i>&rarr;</i></a>
                        </div> <!-- .all-news-item -->
                    <?php endforeach ?> 

                    </div> <!-- .all-news -->

<!--                     <div class="pagination">
                        <ul>
                            <li class="page-selected"><a>1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#" class="next">&rarr;</a></li>
                        </ul>
                    </div> <!-- .pagination --> 

                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->