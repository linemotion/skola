<?php

class News extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_news');

		$this->module = "news";
	}
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$image = modules::run('gallery/get_by_id', $this->uri->segment(4));

		$this->db->where("id" , $this->uri->segment(4));
		$this->db->delete("gallery");

		redirect('/cms/news/gallery/'.$image->news_id);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			$data["image"] = modules::run('resize/upload',"news");

		$data['news_id'] = $this->uri->segment(4);

		$this->db->insert("gallery",$data);

		redirect('/cms/news/gallery/'.$data['news_id']);
	}	
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['gallery'] = modules::run("gallery/get_where_custom","news_id",$this->uri->segment(4));

		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}

	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$id = $this->uri->segment(3);
		//$data['category'] = modules::run("categories/get_by_id",$id);
		$data['news'] = modules::run("news/get","id");

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		//$data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}

	function index()
	{
		// $new = modules::run("news/get_max");

		// redirect("/vesti/".url_title(rs_char(cirlat($new->name)))."/".$new->id);
		$data['breadcrumbs'] = array(
			array("name" => "Вести","link" => "/vesti")
			); 	

		$data['news'] = modules::run("news/get","id");
		//$data['products'] = modules::run("products/get_by_action");
	    $data['useful'] = modules::run('subcategories/get_with_limit',3,0,"id");
	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);


		echo modules::run('template/render',$this->module,"index",$data);
	}

	function review()
	{
		$id = $this->uri->segment(3);
		$data['new'] = modules::run("news/get_by_id",$id);

	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);
		$data['news'] = modules::run("news/get","id");
		$data['gallery'] = modules::run("gallery/get_where_custom","news_id",$id);
			
		// echo "<pre>";
		// print_r($data);
		// die();


		$data['breadcrumbs'] = array(
			array("name" => "Вести","link" => "/vesti"),
			array("name" => $data['new']->name,"link" => "/proizvodi/")
			); 	

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$data['name'] = $this->input->post('name');
		// $data['desc'] = $this->input->post('desc');
		// $data['date'] = $this->input->post('date');
		// $data['desc'] = $this->get_max_pos()->position + 1;

		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/cms/news/admin');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(4));

		redirect('/cms/news/admin/');
	}
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$sorted = $this->input->post("data");

		foreach ($sorted as $key => $value)
		{
			$data['position'] = $value;

			$this->db->where("id",$key);
			$this->db->update("categories",$data);	
		}
		
		//$category = $this->get_by_id($id);

		// echo "success"; 
	}
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(4);

		$data['categories'] = modules::run("categories/get","id");

		$data['new'] = $this->get_by_id($id);

		// $data['breadcrumbs'] = array(
		// 			array("name" => "Proizvodi","link" => "/admin/products"),
		// 			array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id)
		// );
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(4);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['date'] = $this->input->post('date');
		$data['desc'] = $this->input->post('desc');

		if($_FILES['userfile']['error'] == 0)
		{
			modules::run('resize/delete',$this->module,$category->image);
			$data['image'] = modules::run('resize/upload',$this->module);		
		}

		$this->_update($id,$data);

		redirect('/cms/news/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_news->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_news->get_by_url($url);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_news->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_news->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_news->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_news->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_news->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_news->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_news->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_news->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_news->get_max();
		return $max_id;
	}
	function get_home() 
	{
		$new = $this->mdl_news->get_home();
		return $new;
	}
	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_news->_custom_categories($mysql_categories);
		return $categories;
	}

}