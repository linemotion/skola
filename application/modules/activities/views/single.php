        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="inside-sidebar">
                        <ul class="inside-cats">
                <?php foreach ($activities as $item): ?>
                    <?php if ($item->id == $this->uri->segment(3)): ?>
                            <li class="ic-selected">
                    <?php else: ?>
                            <li>
                    <?php endif ?>
                                <a href="/aktivnosti/<?php echo url_title(rs_char(cirlat($item->name))); ?>/<?php echo $item->id ?>">
                                    <small><?php echo $item->date ?></small>
                                    <span><?php echo $item->name ?></span>
                                </a>
                            </li>
                <?php endforeach ?>
                        </ul>
                        <a href="/aktivnosti" class="btn-default btn-reverse"><i>&larr;</i> Све активности</a>
                    </div> <!-- .inside-sidebar -->
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <article class="inside-article">
                        <h1><?php echo $activity->name ?> <span class="article-date"><i class="icon-date"></i> <?php echo $activity->date ?></span></h1>
<?php if ($activity->image): ?>
                           <a href="/img/activities/<?php echo $activity->image ?>" class="fancybox"><img src="/img/activities/<?php echo $activity->image ?>" class="img-top-left"></a>                         
                            <?php echo $activity->desc ?>
<?php else: ?>
    <?php if ($gallery): ?>
                            <?php echo $activity->desc ?>
                            <div class="gallery">
                                <ul class="gallery-list group">
    <?php foreach (modules::run("activities_gallery/get_where_custom","activities_id",$this->uri->segment(3)) as $image): ?>
                                    <li><a href="/img/activities/<?php echo $image->image ?>" rel="gallery" class="fancybox"><img src="/img/activities/thumbs/<?php echo $image->image ?>"></a></li>    
    <?php endforeach ?>
                                </ul>
                            </div> <!-- .gallery -->
                            
    <?php else: ?>        
                            <?php echo $activity->desc ?>                    
    <?php endif ?>

<?php endif ?> 
                    </article> <!-- .inside-article -->
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->
