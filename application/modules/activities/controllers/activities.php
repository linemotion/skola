<?php

class Activities extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_activities');

		$this->module = "activities";
	}
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$image = modules::run('activities_gallery/get_where_custom',"id", $this->uri->segment(4));

		$this->db->where("id" , $this->uri->segment(4));
		$this->db->delete("activities_gallery");

		redirect('/cms/activities/gallery/'.$image->activities_id);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			$data["image"] = modules::run('resize/upload',"activities");

		$data['activities_id'] = $this->uri->segment(4);

		$this->db->insert("activities_gallery",$data);

		redirect('/cms/activities/gallery/'.$data['activities_id']);
	}	
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['gallery'] = modules::run("activities_gallery/get_where_custom","activities_id",$this->uri->segment(4));

		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}

	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['activities'] = modules::run("activities/get","id");

		// $data['breadcrumbs'] = array(
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$categories = modules::run("categories/get","position");

		foreach ($categories as $category) 
		{
			$data['url'] = url_title(rs_char($category->name));
			$data['image'] = url_title(rs_char($category->name)).".jpg";

			$this->_update($category->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}
	
	function index()
	{
		// $activity = modules::run("activities/get_max");

		// redirect("/aktivnosti/".url_title(rs_char(cirlat($activity->name)))."/".$activity->id);
		$data['breadcrumbs'] = array(
			array("name" => "Активности","link" => "/aktivnosti")
			); 	

		$data['activities'] = modules::run("activities/get","id");
		//$data['products'] = modules::run("products/get_by_action");
	    $data['useful'] = modules::run('subcategories/get_with_limit',3,0,"id");
	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);


		echo modules::run('template/render',$this->module,"index",$data);
	}


	function review()
	{
		$id = $this->uri->segment(3);
		$data['activity'] = modules::run("activities/get_by_id",$id);
		
	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);
		$data['activities'] = modules::run("activities/get","id");
		$data['gallery'] = modules::run("activities_gallery/get_where_custom","activities_id",$id);
		//$news_id = $this->uri->segment(3);
		
		// echo "<pre>";
		// print_r($data);
		// die();

		//$data['images'] = modules::run("gallery/get_where_custom","news_id",1);


		$data['breadcrumbs'] = array(
			array("name" => "Активности","link" => "/aktivnosti"),
			array("name" => $data['activity']->name,"link" => "/aktivnosti/".$id)
			); 	

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$data['name'] = $this->input->post('name');
		// $data['link'] = $this->input->post('link');
		// $data['position'] = $this->get_max_pos()->position + 1;

		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/cms/activities/admin');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(4));

		redirect('/cms/activities/admin');
	}
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$sorted = $this->input->post("data");

		foreach ($sorted as $key => $value)
		{
			$data['position'] = $value;

			$this->db->where("id",$key);
			$this->db->update("categories",$data);	
		}
		
		//$category = $this->get_by_id($id);

		// echo "success"; 
	}
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(4);

		$data['act'] = $this->get_by_id($id);

		// $data['breadcrumbs'] = array(
		// 			array("name" => "Proizvodi","link" => "/admin/products"),
		// 			array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id)
		// );
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(4);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['desc'] = $this->input->post('desc');
		$data['date'] = $this->input->post('date');

		if($_FILES['userfile']['error'] == 0)
		{
			modules::run('resize/delete',$this->module,$category->image);
			$data['image'] = modules::run('resize/upload',$this->module);		
		}

		$this->_update($id,$data);

		redirect('cms/activities/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_activities->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_activities->get_by_url($url);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_activities->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_activities->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_activities->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_activities->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_activities->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_activities->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_activities->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_activities->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_activities->get_max();
		return $max_id;
	}
	function get_home() 
	{
		$new = $this->mdl_activities->get_home();
		return $new;
	}
	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_activities->_custom_categories($mysql_categories);
		return $categories;
	}

}