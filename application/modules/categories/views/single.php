        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="inside-sidebar">
                        <ul class="inside-cats">
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $this->uri->segment(3)): ?>
                            <li class="ic-selected">
                    <?php else: ?>
                            <li>
                    <?php endif ?>
                                <a href="/<?php echo url_title(rs_char(cirlat($item->cat_name))); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
<?php if ($this->uri->segment(1) == "bilingvalna-nastava"): ?>
    <?php if ($this->session->userdata("lang") == "lat"): ?>
                                        <span><?php echo cirlat_front($item->name); ?></span>
    <?php else: ?>                                        
                                        <span><?php echo $item->name; ?></span>
    <?php endif ?>
<?php else: ?>
                                        <span><?php echo $item->name; ?></span>
<?php endif ?>                                        
                                </a>
                            </li>
                <?php endforeach ?>
            </ul>
                    </div> <!-- .inside-sidebar -->
                    <h5 class="feat-links-aside-title">Корисни линкови:</h5>
                    <ul class="feat-links-aside">
                    <?php foreach ($useful as $links): ?>
                        <li>
                            <a href="/<?php echo url_title(rs_char(cirlat($links->cat_name))) ?>/<?php echo $links->url ?>/<?php echo $links->id ?>">
                                <i class="icon-<?php echo $links->icon; ?>"></i>
                                <span><?php echo $links->name; ?></span>
                            </a>
                        </li>
                    <?php endforeach ?>                    
                    </ul>
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
                    <article class="inside-article">
<?php if ($this->uri->segment(1) == "bilingvalna-nastava"): ?>
                       
<?php if ($this->session->userdata("lang") == "lat"): ?>
                             <h1><?php echo cirlat_front($category->name) ?>
                            <a href="/user/cir" class="lang-switch">Ћирилица</a></h1>
                            <?php echo cirlat_front($category->desc);?>  
<?php else: ?>
                             <h1><?php echo $category->name ?>
                            <a href="/user/lat" class="lang-switch">Latinica</a></h1>
                            <?php echo $category->desc;?>  
<?php endif ?>  
<?php else: ?>
                        <h1><?php echo $category->name ?></h1>
                        <?php echo $category->desc;?>                          
<?php endif ?>                                     
                    </article> <!-- .inside-article -->                
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->