
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Vitorog promet - Black Red White - nameštaj, dnevne sobe, kreveti, stolice...</title>
        <meta name="description" content="Vitorog promet - nameštaj za vaš dom: dnevne sobe, kreveti, kuhinje, stolice. Pristupačne cene i vrhunski kvalitet. Black Red White brend.">
        <meta name="viewport" content="width=device-width">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/reset.css">
        <link rel="stylesheet" href="/assets/css/main.css">
        <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

        <!-- fb code start -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=554978697864319";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!-- fb code end -->

        <div class="page">

            <header class="site-header">

                <nav class="small-nav group">

                    <ul>
                        <li><a href="/">Naslovna</a></li>
                        <li><a href="/o-nama">O nama</a></li>
                        <li><a href="/lokacije">Lokacije</a></li>
                    </ul>

                </nav>

                <nav class="main-nav group">

                    <a href="/" class="nav-home">
                        <img src="/assets/img/vitorog-logo.png" alt="">
                    </a>

                    <ul class="group">
<?php if ($this->uri->segment(1) == "proizvodi"): ?>
                        <li class="nav-proizvodi nav-selected"><a href="/proizvodi">Proizvodi</a>

<?php else: ?>
                        <li class="nav-proizvodi"><a href="/proizvodi">Proizvodi</a>

<?php endif ?>
                            <ul class="sub-nav">
<?php foreach (modules::run("categories/get","position") as $category): ?>   
                                <li>
                                    <a href="/proizvodi/<?php echo $category->url;?>"><?php echo $category->name?></a>
                                    <ul class="sub-sub-nav">
    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                                        <li><a href="/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->name?></a>  </li>
    <?php endforeach; ?>
                                    </ul>
                                </li> 
<?php endforeach; ?>

                            </ul>
                        </li>
<?php if ($this->uri->segment(1) == "black-red-white"): ?>
                        <li class="nav-brw nav-selected"><a href="/black-red-white">Black Red White <small>BRW</small></a>

<?php else: ?>
                        <li class="nav-brw"><a href="/black-red-white">Black Red White <small>BRW</small></a>

<?php endif ?>                        
                            <ul class="sub-nav">

    <?php foreach (modules::run('subcategoriesBRW/get_where_custom','cat_id',39) as $subcategory): ?>
                                        <li><a href="/black-red-white/<?php echo url_title(rs_char($subcategory->name));?>/<?php echo $subcategory->id;?>"><?php echo $subcategory->name?></a>  </li>
    <?php endforeach; ?>

                            </ul>
                        </li>
<?php if ($this->uri->segment(1) == "studio-vito"): ?>
                        <li class="nav-studio nav-selected"><a href="/studio-vito">Studio Vito</a></li>

<?php else: ?>
                        <li class="nav-studio"><a href="studio-vito">Studio Vito</a></li>

<?php endif ?>                        
<?php if ($this->uri->segment(1) == "akcije"): ?>
                        <li class="nav-akcije nav-selected"><a href="/akcije">Akcije</a></li>

<?php else: ?>
                        <li class="nav-akcije"><a href="/akcije">Akcije</a></li>

<?php endif ?>                        
<?php if ($this->uri->segment(1) == "servis"): ?>
                        <li class="nav-servis nav-selected"><a href="/servis">Servis</a></li>

<?php else: ?>
                        <li class="nav-servis"><a href="/servis">Servis</a></li>

<?php endif ?> 
<?php if ($this->uri->segment(1) == "kontakt"): ?>
                        <li class="nav-kontakt nav-selected"><a href="/kontakt">Kontakt</a></li>

<?php else: ?>
                        <li class="nav-kontakt"><a href="/kontakt">Kontakt</a></li>

<?php endif ?> 
                    </ul>

                </nav>

                <div class="bread-search group">

                    <nav class="breadcrumbs">

                        <ul>
                            <li class="bc-home"><a href="/" class="ir">Početak</a></li>
                            <li class="bc-selected"><a>Proizvodi</a></li>
                        </ul>

                    </nav>

                    <div class="search">
                        <form class="stdform" action="/products/pagination" method="post" enctype="multipart/form-data">
                            <input type="text" name="search" class="search-field" placeholder="Pretraga">
                            <button type="submit" class="search-submit"></button>
                        </form>
                    </div> <!-- .search -->

                </div> <!-- .bread-search -->

            </header> <!-- .site-header -->



            <div class="content content-inside">

                <div class="group">

                    <aside class="sidebar col">

                        <h3 class="subtitle">
                            Rezultati pretrage:
                        </h3>

                        <nav class="aside-nav">

                            <ul>
                                <li class="aside-selected">
                                    <a href="/proizvodi/pretraga/<?php echo $this->uri->segment(3); ?>">Proizvodi (45)</a>
                                </li>
                                <li>
                                    <a href="/grupe/pretraga/<?php echo $this->uri->segment(3); ?>">Grupe (12)</a>
                                </li>
                                <li>
                                    <a href="/podgrupe/pretraga/<?php echo $this->uri->segment(3); ?>">Podgrupe (4)</a>
                                </li>
                            </ul>

                        </nav>

                    </aside>

                    <section class="main col">

                        <h2 class="section-title search-title">Pronađeni <b>proizvodi</b> za pojam "<b><?php echo $search; ?></b>":</h2>


<?php $categories = modules::run("categories/get_where_paginateSearch",$this->uri->segment(3),12,$this->uri->segment(4,0)); ?>
<?php for($i=0; $i < count($categories); $i++): ?>
    <?php if ($i%2==0): ?>
        <div class="cat-grid group">
            <div class="col-1-2 alpha">
    <?php else: ?>
             <div class="col-1-2 omega">   
    <?php endif ?>

            <a href="/proizvodi/<?php echo $categories[$i]->url;?>">
                <?php echo $categories[$i]->name?>
                <img src="/assets/img/temp/cat-img.jpg" alt="">
            </a>

            </div>
    <?php if ($i%2 == 1): ?>
    </div> <!-- group of 2 categories -->
    <?php endif ?>

<?php endfor; ?>
    <?php if ($i%2 == 1): ?>
    </div> <!-- group of 2 categories -->
    <?php endif ?>


                    <?php echo modules::run('pagination/paginate_search_categories'); ?>                        
                        
                    </section>

                </div> <!-- main + sidebar -->

                <footer class="page-footer">

                    <div class="group">

                        <div class="footer-nav">

                            <nav class="fn-subnav">
                                <ul>
                                    <li><a href="#">Naslovna</a></li>
                                    <li><a href="#">O nama</a></li>
                                    <li><a href="#">Lokacije</a></li>
                                </ul>
                            </nav>

                            <nav class="fn-nav">
                                <ul>
                                    <li><a href="#">Proizvodi</a></li>
                                    <li><a href="#">Black Red White</a></li>
                                    <li><a href="#">Studio Vito</a></li>
                                    <li><a href="#">Akcije</a></li>
                                    <li><a href="#">Servis</a></li>
                                    <li><a href="#">Kontakt</a></li>
                                </ul>
                            </nav>

                        </div> <!-- .footer-nav -->

                        <div class="footer-meta group">

                            <div class="fm-fb">

                                <div class="fb-like" data-href="http://www.facebook.com/vitorog" data-send="false" data-layout="box_count" data-width="53" data-show-faces="false" data-font="tahoma"></div>

                            </div> <!-- .fm-fb -->

                            <div class="fm-info">

                                <ul>
                                    <li class="fm-mail">
                                        <a href="mailto:office@vito.rs">office@vito.rs</a>
                                    </li>
                                    <li class="fm-phone"><span>011-6556-334</span> <span>021-427-433</span></li>
                                </ul>


                            </div> <!-- .fm-info -->


                        </div> <!-- .footer-meta -->

                    </div>

                </footer>

            </div> <!-- .content -->

        </div> <!-- .page -->

        <footer class="site-footer">

            <div class="page group">

                <a href="/" class="logo-footer">
                    <img src="/assets/img/logo-footer.png" alt="Vitorog promet">
                </a>

                <p class="meta-footer">
                    &copy; 2013 Vitorog Promet d.o.o.<br>
                    Sva prava zadržana.
                </p>

            </div>

        </footer>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="/assets/js/plugins.js"></script>
        <script src="/assets/js/default.js"></script>

    </body>
</html>

