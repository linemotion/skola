        <section class="main">

            <header class="main-header group">
                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Kategorije</h2>
                    <a href="#" class="btn-add md-trigger" data-modal="modal-add-group"><i class="fa fa-plus-circle"></i> Dodaj kategoriju</a>
                </div>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">Kategorije</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime kategorije</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">

<?php foreach ($categories as $category): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/cms/categories/edit/<?php echo $category->id; ?>"><?php echo $category->name;; ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/cms/categories/delete/<?php echo $category->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-group" data-id="<?php echo $category->id; ?>" id="delete_cat_action" data-controller="categories"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/cms/categories/edit/<?php echo $category->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                    <a href="#" class="move-btn" data-id="<?php echo $category->id; ?>" ><i class="fa fa-arrows"></i></a>
                                </td>
                            </tr>
<?php endforeach ?>
                       </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-add-group">
            <div class="md-content">
                <h3>Dodaj grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime grupe</h4>

                        <form action="/cms/categories/create" method="Post">

                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>
                        
                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-group">
            <div class="md-content md-content-del">
                <h3>Obriši grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu grupu?</h4>

                        <form action="" method="Post" id="delete_cat_form">
                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-del-large " ><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>