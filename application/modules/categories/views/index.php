<section class="main col">



<?php for($i=0;$i < 12;$i+=2): ?>
    <div class="cat-grid group">
        <div class="col-1-2 alpha">
            <a href="/proizvodi/<?php echo $categories[$i]->url;?>">
                <?php echo $categories[$i]->name?>
                <img src="/temp/categories/<?php echo $categories[$i]->image;?>?rand(1,3000)" alt="">
            </a>
        </div>

        <div class="col-1-2 omega">
            <a href="/proizvodi/<?php echo $categories[$i+1]->url;?>">
                <?php echo $categories[$i+1]->name?>
                <img src="/temp/categories/<?php echo $categories[$i+1]->image?>?rand(1,3000)" alt="">
            </a>
        </div>
    </div> <!-- .cat-grid -->
<?php endfor; ?>

</section>