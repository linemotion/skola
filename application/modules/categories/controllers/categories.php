<?php

class Categories extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_categories');

		$this->module = "categories";

		if (!$this->session->userdata("lang"))
		{
			$sess_data['lang'] = "cir";
			$this->session->set_userdata($sess_data);
		}

	}


	function content()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/proizvodi")
			); 		
		$data['category'] = modules::run("categories/get_by_id",$this->uri->segment(4));

		echo modules::run('template/admin_render',$this->module,"content",$data);
	}

	function get_num_search($search) 
	{
		$num = $this->mdl_categories->get_num_search($search);
		return $num;
	}
	function get_footer_nosub() 
	{
		$num = $this->mdl_categories->get_footer_nosub();
		return $num;
	}
	function get_footer_sub() 
	{
		$num = $this->mdl_categories->get_footer_sub();
		return $num;
	}


	function search()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Grupe","link" => "/proizvodi"),
			array("name" => "Pretraga","link" => ""),
			array("name" => $this->uri->segment(3),"link" => "")
			); 
		$data['search'] = $this->uri->segment(3);

		//$data['products'] = $this->get_by_search($search);
		$this->load->view('search',$data);

	}
	function get_where_paginateSearch($search,$per_page,$offset) 
	{
		$categories = $this->mdl_categories->get_where_paginateSearch($search,$per_page,$offset);

		return $categories;
	}
	function index()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/proizvodi")
			); 		
		$data['categories'] = modules::run("categories/get","id");

		echo modules::run('template/render',$this->module,"index",$data);
	}
	function admin()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/proizvodi")
			); 		
		$data['categories'] = modules::run("categories/get","id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}

	function collective()
	{
		redirect("/kolektiv/rukovodstvo-skole/13");
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$categories = modules::run("categories/get","position");

		foreach ($categories as $category) 
		{
			$data['url'] = url_title(rs_char($category->name));
			$data['image'] = url_title(rs_char($category->name)).".jpg";

			$this->_update($category->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}

	function review()
	{
		$url = $this->uri->segment(1);
		$data['category'] = modules::run("categories/get_by_url",$url);

		$data['breadcrumbs'] = array(
			array("name" => $data['category']->name,"link" => "/".$url)
			); 
	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);
	    $data['useful'] = modules::run('subcategories/get_with_limit',3,0,"id");

		$data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$data['name'] = $this->input->post('name');
		//$data['position'] = $this->get_max_pos()->position + 1;

		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/cms/categories/admin');
	}

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		$data['category'] = $this->get_by_id($id);
		$data['subcategories'] = modules::run("subcategories/get_where_custom", "cat_id", $id);
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		//$data['category'] = $this->get_by_id($id);
		
		$this->_delete($id);
		redirect("cms/categories/admin");
	}


	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(4);
		$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['desc'] = $this->input->post('desc');
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$category->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/cms/categories/content/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_categories->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_categories->get_by_url($url);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_categories->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_categories->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_categories->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_categories->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_categories->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_categories->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_categories->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_categories->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_categories->get_max();
		return $max_id;
	}

	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_categories->_custom_categories($mysql_categories);
		return $categories;
	}

}