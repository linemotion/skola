<?php

class User extends MX_Controller
{

    function  __construct()
    {
		parent::__construct();
		$this->load->model('Mdl_user');
    }

	public function index($errors = false)
	{
		$data["errors"] = $errors;

	    $this->load->view("login",$data);
	}

	public function logged_in()
	{
	    if(!$this->session->userdata('logged_in') == true)
		redirect('prijava');
	}	

    function do_login()
    {

		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			redirect('prijava');
		}
		else
		{
		    if($this->Mdl_user->validate())
		    {
				$sess_data['username']     = $this->input->post('username');
				$sess_data['logged_in'] = true;

				$this->session->set_userdata($sess_data);

				redirect('admin/dashboard');
		    }
		    else
		    {
				$this->index(true);
		    }
		}
	}

    function logout()
    {
		$this->session->unset_userdata("username");
		$this->session->unset_userdata('logged_in');

		redirect("/");
    }
    function lat()
    {
		$sess_data['lang'] = "lat";

		$this->session->set_userdata($sess_data);

		redirect("/bilingvalna-nastava");
    }
    function cir()
    {
		$sess_data['lang'] = "cir";

		$this->session->set_userdata($sess_data);

		redirect("/bilingvalna-nastava");
    }

}
