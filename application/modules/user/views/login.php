<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Xгимнаѕија пријава</title>
<link rel="stylesheet" href="/admin_assets/admin-css/style.default.css" type="text/css" />
<link id="addonstyle" rel="stylesheet" href="/admin_assets/admin-css/style.contrast.css" type="text/css" />

<script type="text/javascript" src="/admin_assets/admin-js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="/admin_assets/admin-js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="/admin_assets/admin-js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="/admin_assets/admin-js/plugins/jquery.uniform.min.js"></script>
<script type="text/javascript" src="/admin_assets/admin-js/custom/general.js"></script>
<script type="text/javascript" src="/admin_assets/admin-js/custom/index.js"></script>
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="admin-css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="admin-css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<body class="loginpage">

    <div class="loginbox">
        <div class="loginboxinner">
            
            <div class="logo">
                <h1><span>X гимназија</span></h1>
                <p>администрација сајта</p>
            </div><!--logo-->
            
            <br clear="all" /><br />

            <?php if ($errors): ?>
                
                Погрешно корисничко име или/и шифра
                
            <?php endif ?>

          </div><!--nopassword-->
            
            <form id="login" action="/user/do_login" method="post">
                
                <div class="username">Корисничко име: 
                    <div class="usernameinner">
                        <input type="text" name="username" id="username" />
                    </div>
                </div>
                
                <div class="password">Шифра: 
                    <div class="passwordinner">
                        <input type="password" name="password" id="password" />
                    </div>
                </div>
                
                <button style="margin-left: 0;">Пријави се</button>
                
                <!--<div class="keep"><input type="checkbox" /> Zapamti me</div>-->
            
            </form>
            
        </div><!--loginboxinner-->
    </div><!--loginbox-->


</body>
</html>
