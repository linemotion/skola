<?php

class Template extends MX_Controller
{
    function __construct()
    {
		parent::__construct();
    }

    public function index()
    {
		$this->load->view('template');
    }

    public function breadcrumbs($breadcrumbs)
    {
        $data['breadcrumbs'] = $breadcrumbs;

        $this->load->view('public/breadcrumbs',$data);
    }            

    public function admin()
    {
        $this->load->view('admin/template');
    }

    public function admin_header()
    {
        $this->load->view('admin/header');
    }

    public function admin_footer()
    {
        $this->load->view('admin/footer');
    }
    
    public function render($module,$view,$data="")
    {
    	$data['module'] = $module;
    	$data['view'] = $view;

		$this->load->view('public/template',$data);
    }
    public function admin_render($module,$view,$data="")
    {
        $data['module'] = $module;
        $data['view'] = $view;

        $this->load->view('admin/template',$data);
    }
}