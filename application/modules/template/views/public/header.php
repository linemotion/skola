<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Десета гимназија - Михајло Пупин</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/additional.css">
        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

        <!-- facebook code -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=554978697864319&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!-- end facebook code -->

        <header class="site-header">
            <div class="container group">
                <h1 class="site-logo">
                    <a href="/"><img src="/img/xgimn-logo.png" alt="Десета гимназија - Михајло Пупин"></a>
                </h1>
                <nav class="site-nav">
                    <ul class="site-nav-small">
                        <li><a href="/">Насловна</a></li>                     
                <?php foreach ($categories as $categ): ?>
                        <?php if ($categ->url == $this->uri->segment(1)): ?>
                            <li class="site-nav-active">
                        <?php else: ?>
                            <li>
                        <?php endif ?>
                            <a href="/<?php echo $categ->url ?>"><?php echo $categ->name; ?></a></li>
                <?php endforeach ?>
                    </ul>
                    <ul class="site-nav-main">
                <?php foreach ($featured_cat as $cat): ?>
                       <?php if ($cat->url == $this->uri->segment(1)): ?>
                            <li class="site-nav-active">
                        <?php else: ?>
                            <li>
                        <?php endif ?>
                            <a href="/<?php echo $cat->url ?>"><?php echo $cat->name; ?></a></li>                
                <?php endforeach ?>
                    </ul>
                </nav> <!-- .site-nav -->
            </div> <!-- .container -->
        </header> <!-- .site-header-->

        <div class="site-subheader">
            <div class="container group">

            <?php echo modules::run("/template/breadcrumbs", $breadcrumbs) ?>

                <div class="subheader-social">
                    <div>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
                    </div>
                    <div>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                    </div>
                </div> <!-- .subheader-social -->
            </div> <!-- .container -->
        </div> <!-- .site-subheader -->