        <footer class="site-pre-footer">
            <div class="container">
                <nav class="footer-nav group">
                    <ul>
                        <li><a href="/">Насловна</a></li>
<?php foreach (modules::run("categories/get_footer_nosub") as $nosub): ?>
                        <li><a href="/<?php echo $nosub->url ?>"><?php echo $nosub->name; ?></a></li>                           
<?php endforeach ?>
                    </ul>
<?php foreach (modules::run("categories/get_footer_sub") as $sub): ?>
                    <ul>
                        <li><a href="/<?php echo $sub->url ?>"><?php echo $sub->name; ?></a></li>
<?php foreach (modules::run("subcategories/get_where_custom","cat_id",$sub->id) as $subcategory): ?>
                        <li><a href="/<?php echo $sub->url ?>/<?php echo $subcategory->url ?>/<?php echo $subcategory->id ?>"><?php echo $subcategory->name; ?></a></li>                                               
<?php endforeach ?>    
                    </ul>                 
<?php endforeach ?>
             </nav>
            </div> <!-- .container -->
        </footer> <!-- .site-pre-footer -->

        <footer class="site-footer">
            <div class="container group">
                <div class="footer-meta">
                    <p class="footer-meta-info">
                        Десета Гимназија – Антифашистичке борбе 1а, 011/2135-722<br>
                        <a href="mailto:kontakt@xgimnazija.edu.rs">kontakt@xgimnazija.edu.rs</a>
                    </p>
                    <p class="footer-social">
                        <a href="https://www.facebook.com/groups/1508541572761066/?fref=ts"><i class="icon-facebook"></i></a>
                        <a href="https://www.youtube.com/channel/UC9O1TnW3WenulgjxEyoD0fA"><i class="icon-youtube"></i></a>
                    </p>
                </div> <!-- .footer-meta -->
                <div class="footer-friends">
                    <a href="#"><img src="/img/institut.png" alt=""></a>
                    <a href="#"><img src="/img/ministarstvo.png" alt=""></a>
                </div> <!-- .footer-friends -->
            </div> <!-- .container -->
        </footer> <!-- .site-footer -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="/js/vendor/enquire.min.js"></script>
        <script src="/js/vendor/jquery.fancybox.pack.js"></script>

        <script src="/js/default.js"></script>
        <?php if ($this->uri->segment(1) == "kontakt"): ?>
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <script src="/js/mapa.js"></script>                
        <?php endif ?>
    </body>
</html>
