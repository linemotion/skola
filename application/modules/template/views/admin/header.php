<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
            <aside class="sidebar">

                <header class="sidebar-header">
                    <h1 class="sidebar-logo group">
                        <img src="/admin-assets/img/logo.png" alt="">
                        <span><b>Okov</b> Administracija</span>
                    </h1>
                </header>

                <nav class="sidebar-nav">
                    <ul>
                        <li class="sidebar-nav-selected">
                            <a href="/cms/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                        </li>
                        <li>
                            <a href="/cms/categories/admin"><i class="fa fa-lg fa-picture-o"></i>Kategorije</a>
                        </li>
                        <li>
                            <a href="/cms/activities/admin"><i class="fa fa-lg fa-tag"></i>Aktivnosti</a>
                        </li>
                        <li>
                            <a href="/cms/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                        </li>
                    </ul>
                </nav>

                <footer class="copy-footer">
                    CMS Copyright &copy; <a href="www.linemotion.com">Linemotion</a>
                </footer>

            </aside> <!-- .sidebar -->


