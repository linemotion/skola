<?php

class Sorts extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_sorts');
		$this->module = "sorts";

		if (!$this->session->userdata("lang"))
		{
			$sess_data['lang'] = "cir";
			$this->session->set_userdata($sess_data);
		}

	}

	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$image = modules::run('sorts_gallery/get_by_id', $this->uri->segment(4));

		$this->db->where("id" , $this->uri->segment(4));
		$this->db->delete("sorts_gallery");

		redirect('/cms/sorts/gallery/'.$image->sort_id);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		if ($this->input->post("group"))
		{
			$data['group_id'] = $this->input->post("group");
			$sort = modules::run("sorts/get_by_id",$this->uri->segment(4));
			
			if($_FILES['userfile']['error'] == 0)
				$data["image"] = modules::run('resize/upload',"sorts",$sort->url);
		}
		else
		{
			$sort = modules::run("sorts/get_by_id",$this->uri->segment(4));

			if($_FILES['userfile']['error'] == 0)
				$data["image"] = modules::run('resize/upload',"sorts",$sort->url);
		}

		$data['sort_id'] = $this->uri->segment(4);



		$this->db->insert("sorts_gallery",$data);
		if($data['sort_id'] != 7)
			redirect('/cms/sorts/gallery/'.$data['sort_id']);
		else
			redirect('/cms/sorts/gallery/'.$data['sort_id']."/".$data['group_id']);

	}	
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 
		$id = $this->uri->segment(4);

		$data['sort'] = $this->get_by_id($id);

		if ($this->uri->segment(5)) 
			$data['gallery'] = modules::run("sorts_gallery/get_where_custom","group_id",$this->uri->segment(5));
		else
			$data['gallery'] = modules::run("sorts_gallery/get_where_custom","sort_id",$this->uri->segment(4));




		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}

	function get_num_by_paginateSearch($search) 
	{
		$num = $this->mdl_sorts->get_num_search($search);
		return $num;
	}
	function get_where_paginateSearch($search,$per_page,$offset) 
	{
		$sorts = $this->mdl_sorts->get_where_paginateSearch($search,$per_page,$offset);

		return $sorts;
	}
	function search()
	{
		$data['search'] = $this->uri->segment(3);
		
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/proizvodi"),
			array("name" => "Pretraga","link" => ""),
			array("name" => $data['search'],"link" => "")
			); 	
		//$data['products'] = $this->get_by_search($search);
		$this->load->view('search',$data);

	}

	function review()
	{
		$id = $this->uri->segment(4);

		$data['sort'] =modules::run("sorts/get_by_id",$id);
		
		$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(1));
		$data['subcategory'] =modules::run("subcategories/get_by_id",$data['sort']->subcat_id);
		$data['subcategories'] =modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);
		$data['sorts'] =modules::run("sorts/get_where_custom","subcat_id",$data['sort']->subcat_id);


		// echo "<pre>";
		// print_r($data);
		// die();
		//uzas jebeni
		// $this->db->where('id',$url);
		// $query = $this->db->get("sorts");
		//kraj uzasa
    	
    	$data['useful'] = modules::run('subcategories/get_with_limit',3,0,"id");
	    $data['featured_cat'] = modules::run('categories/get_where_custom',"featured",1);
	    $data['categories'] = modules::run('categories/get_where_custom',"featured !=",1);


		//$data['subcategory'] = $query->row();
		//$data['sorts'] =modules::run("sorts/get_where_custom","cat_id",$data['subcategory']->cat_id);
		
			$data['breadcrumbs'] = array(
			array("name" => $data['category']->name,"link" => "/".$data['category']->url),
			array("name" => $data['subcategory']->name,"link" => "/".$data['category']->url."/".$data['subcategory']->url."/".$data['subcategory']->id),
			array("name" => $data['sort']->name,"link" => "/")
				); 		
		

		echo modules::run('template/render',$this->module,"single",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$sorts = modules::run("sorts/get","position");

		foreach ($sorts as $subcategory) 
		{
			$data['url'] = url_title(rs_char($subcategory->name));
			$data['image'] = url_title(rs_char($subcategory->name)).".jpg";

			$this->_update($subcategory->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		$data['sort'] = $this->get_by_id($id);
		
		$this->_delete($id);
		redirect("cms/subcategories/edit/".$data["sort"]->subcat_id);
	}

	function create()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['name'] = $this->input->post('name');
		$data['subcat_id'] = $this->uri->segment(4);
		$data['subcat_name'] = modules::run('subcategories/get_by_id',$data['subcat_id'])->name;
		
		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);


		$this->_insert($data);

		redirect('/cms/subcategories/edit/'.$data['subcat_id']);

	}
	function change()
	{

		$id = $this->input->post('id');


		$output = '<select name="sorts" class="uniformselect" id="subgroup"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('cat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}	
	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		$data['sort'] = $this->get_by_id($id);
		// echo "<pre>";
		// print_r($data);
		// die();					// $config['total_rows'] = count(modules::run("products/get_where_custom_admin",'subcat_id',1));

		// echo "<pre>";
		// print_r($config);
		// die();
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(4);

		$sort = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['desc'] = $this->input->post('desc');
		// $data['cat_id'] = $sort->cat_id;
		//$data['subcat_id'] = $sort->subcat_id;
		// $data['cat_name'] = modules::run('categories/get_by_id',$data['cat_id'])->name;
		//$data['subcat_name'] = modules::run('subcategories/get_by_id',$data['subcat_id'])->name;
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$subcategory->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/cms/sorts/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_sorts->get_max_pos();
		return $max_pos;
	}

	function get($order_by)
	{
		$sorts = $this->mdl_sorts->get($order_by);
		return $sorts;
	}


	function get_with_limit($limit, $offset, $order_by) 
	{
		$sorts = $this->mdl_sorts->get_with_limit($limit, $offset, $order_by);
		return $sorts;
	}

	function get_by_id($id)
	{
		$sorts = $this->mdl_sorts->get_where($id);
		return $sorts;
	}

	function get_where_custom($col, $value) 
	{
		$sorts = $this->mdl_sorts->get_where_custom($col, $value);		
		return $sorts;
	}

	function get_by_url($url) 
	{
		$sorts = $this->mdl_sorts->get_by_url($url);		
		return $sorts;
	}

	function _insert($data)
	{
		$this->mdl_sorts->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_sorts->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_sorts->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_sorts->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_sorts->get_max();
		return $max_id;
	}
	function catnames() 
	{
		$subs = $this->get("position");

		foreach ($subs as $sub)
		{
			$data['cat_name'] = modules::run('categories/get_by_id',$sub->cat_id)->name;
			$this->_update($sub->id,$data);
		}
	}
	function _custom_query($mysql_query) 
	{
		$sorts = $this->mdl_sorts->_custom_categories($mysql_query);
		return $sorts;
	}

}