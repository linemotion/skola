        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li  class="sidebar-nav-selected">
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>            </ul>
            </nav>


            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Izmeni novost</h2>
                </div>

                <header class="tab-header group">
                    <a href="/cms/sorts/edit/<?php echo $sort->id ?>" class="tab-1-2 tab-active">Sadržaj novosti</a>
                    <a href="/cms/sorts/gallery/<?php echo $sort->id ?>" class="tab-1-2 tab">Galerija</a>
                </header>

                <form data-parsley-validate action="/cms/sorts/edit_proccess/<?php echo $sort->id ?>" method="post">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime Vrste</h4>

                            <input type="text" name="name" class="txtinput" value="<?php echo $sort->name ?>" required>

                        </div>

                    </div> <!-- .f-block -->


                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Opis </h4>

                            <div class="ckeditor-outer">
                                <textarea value="" id="editor" name="desc" cols="80" rows="5"><?php echo $sort->desc ?></textarea>
                            </div>

                        </div>
                    </div>   
                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->

