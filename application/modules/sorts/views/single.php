        <section class="inside-content">
            <div class="container group">
                <aside class="site-sidebar">
                    <div class="inside-sidebar">
                        <ul class="inside-cats">
<?php if ($this->uri->segment(1) == "bilingvalna-nastava"): ?>
    <?php if ($this->session->userdata("lang") == "lat"): ?>
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $sort->subcat_id): ?>
                            <li class="ic-selected">
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo cirlat_front($item->name); ?></span>
                                </a>
                    <?php if (modules::run("sorts/get_where_custom", "subcat_id",$sort->subcat_id )): ?>
                                 <ul>
                        <?php foreach (modules::run("sorts/get_where_custom", "subcat_id",$item->id ) as $srt): ?>
                            <?php if ($srt->id == $this->uri->segment(4)): ?>
                                <li  class="ic-sub-selected">
                            <?php else: ?>
                                <li>
                            <?php endif ?>
                                    <a href="/<?php echo $srt->cat_name."/".$srt->subcat_name."/".$srt->url."/".$srt->id ?>"><?php echo cirlat_front($srt->name); ?></a></li>
                        <?php endforeach ?>
                                </ul>                       
                    <?php endif ?>
                            </li>
                    <?php else: ?>
                            <li>
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo cirlat_front($item->name); ?></span>
                                </a>
                            </li>
                    <?php endif ?>               
                <?php endforeach ?>
        <?php else: ?>
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $sort->subcat_id): ?>
                            <li class="ic-selected">
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                    <?php if (modules::run("sorts/get_where_custom", "subcat_id",$sort->subcat_id )): ?>
                                 <ul>
                        <?php foreach (modules::run("sorts/get_where_custom", "subcat_id",$item->id ) as $srt): ?>
                            <?php if ($srt->id == $this->uri->segment(4)): ?>
                                <li  class="ic-sub-selected">
                            <?php else: ?>
                                <li>
                            <?php endif ?>
                                    <a href="/<?php echo $srt->cat_name."/".$srt->subcat_name."/".$srt->url."/".$srt->id ?>"><?php echo $srt->name; ?></a></li>
                        <?php endforeach ?>
                                </ul>                       
                    <?php endif ?>
                            </li>
                    <?php else: ?>
                            <li>
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                            </li>
                    <?php endif ?>               
                <?php endforeach ?>            
        <?php endif ?>
<?php else: ?>
                <?php foreach ($subcategories as $item): ?>
                    <?php if ($item->id == $sort->subcat_id): ?>
                            <li class="ic-selected">
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                    <?php if (modules::run("sorts/get_where_custom", "subcat_id",$sort->subcat_id )): ?>
                                 <ul>
                        <?php foreach (modules::run("sorts/get_where_custom", "subcat_id",$item->id ) as $srt): ?>
                            <?php if ($srt->id == $this->uri->segment(4)): ?>
                                <li  class="ic-sub-selected">
                            <?php else: ?>
                                <li>
                            <?php endif ?>
                                    <a href="/<?php echo $srt->cat_name."/".$srt->subcat_name."/".$srt->url."/".$srt->id ?>"><?php echo $srt->name; ?></a></li>
                        <?php endforeach ?>
                                </ul>                       
                    <?php endif ?>
                            </li>
                    <?php else: ?>
                            <li>
                                <a href="/<?php echo url_title(rs_char($item->cat_name)); ?>/<?php echo $item->url ?>/<?php echo $item->id ?>">
                                    <span><?php echo $item->name; ?></span>
                                </a>
                            </li>
                    <?php endif ?>               
                <?php endforeach ?> 
<?php endif ?>
            </ul>
                    </div> <!-- .inside-sidebar -->
                    <h5 class="feat-links-aside-title">Корисни линкови:</h5>
                    <ul class="feat-links-aside">
                    <?php foreach ($useful as $links): ?>
                        <li>
                            <a href="/<?php echo url_title(rs_char($links->cat_name)) ?>/<?php echo $links->url ?>/<?php echo $links->id ?>">
                                <i class="icon-<?php echo $links->icon; ?>"></i>
                                <span><?php echo $links->name; ?></span>
                            </a>
                        </li>
                    <?php endforeach ?>                    </ul>
                </aside> <!-- .site-sidebar -->
                <div class="site-content">
<?php if (modules::run("sorts_gallery/get_where_custom","sort_id",$this->uri->segment(4))): ?>
                    <article class="inside-article">
                        <h1><?php echo $sort->name ?></h1>
    <?php if ($this->uri->segment(4) == 7): ?>
                        <div class="acc-container">
        <?php foreach (modules::run("gall_group/get","id") as $group): ?>
                            <div class="acc-btn">
                                <h3><?php echo $group->name ?> </h3>
                            </div> <!-- .acc-btn -->  
                            <div class="acc-content">
                                <div class="acc-content-inner">
                                    <div class="gallery">
                                        <ul class="gallery-list group">
                <?php foreach (modules::run("sorts_gallery/get_where_custom","group_id",$group->id) as $image): ?>
                                            <li><a href="/img/sorts/<?php echo $sort->url ?>/<?php echo $image->image ?>" rel="gallery" class="fancybox"><img src="/img/sorts/<?php echo $sort->url ?>/thumbs/<?php echo $image->image ?>"></a></li>                    
                <?php endforeach ?>
                                        </ul>
                                    </div> <!-- .gallery -->
                                </div> <!-- .acc-content-inner -->
                            </div> <!-- .acc-content -->
        <?php endforeach ?>
                        </div> <!-- .acc-container -->
    <?php else: ?>
                        <div class="gallery">
                            <ul class="gallery-list group">
<?php foreach (modules::run("sorts_gallery/get_where_custom","sort_id",$this->uri->segment(4)) as $image): ?>
                                <li><a href="/img/sorts/<?php echo $sort->url ?>/<?php echo $image->image ?>" rel="gallery" class="fancybox"><img src="/img/sorts/<?php echo $sort->url ?>/thumbs/<?php echo $image->image ?>"></a></li>    
<?php endforeach ?>
                            </ul>
                        </div> <!-- .gallery -->
                    </article>
    <?php endif ?>       
<?php else: ?>
                    <article class="inside-article">
<?php if ($this->uri->segment(1) == "bilingvalna-nastava"): ?>
                       
<?php if ($this->session->userdata("lang") == "lat"): ?>
                             <h1><?php echo cirlat_front($sort->name) ?>
                            <a href="/user/cir" class="lang-switch">Ћирилица</a></h1>
                            <?php echo cirlat_front($sort->desc);?>  
<?php else: ?>
                             <h1><?php echo $sort->name ?>
                            <a href="/user/lat" class="lang-switch">Latinica</a></h1>
                            <?php echo $sort->desc;?>  
<?php endif ?>  
<?php else: ?>
                        <h1><?php echo $sort->name ?></h1>
                        <?php echo $sort->desc;?>                          
<?php endif ?>  
                    </article> <!-- .inside-article -->
<?php endif ?>
                </div> <!-- .site-content -->
            </div> <!-- .container -->
        </section> <!-- .home-content -->