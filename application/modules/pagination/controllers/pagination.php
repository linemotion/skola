<?php

class Pagination extends MX_Controller
{


    function __construct()
    {
		parent::__construct();
		$this->load->library('paginate');
    }

	public function paginateView($module="",$method="",$item="products")
	{
    	$offset = $this->uri->segment(4);
    	$id = $this->uri->segment(3);

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$module.'/'.$method.'/'.$id;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run($item."/get_where_custom",'subcat_id',$id));
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['display_pages'] = FALSE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];
		$data['counter_pg'] = $data["offset_pg"] + count(modules::run("products/get_where_paginateView",$id,12,$this->uri->segment(4)));

		$this->load->view('pagination',$data);
	}
	public function paginateViewAdmin($module="",$method="",$item="products")
	{
    	$offset = $this->uri->segment(4);
    	$id = $this->uri->segment(3);

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$module.'/'.$method.'/'.$id;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run($item."/get_where_custom_admin",'subcat_id',$id));
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['display_pages'] = FALSE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];
		$data['counter_pg'] = $data["offset_pg"] + count(modules::run("products/get_where_paginateViewAdmin",$id,12,$this->uri->segment(4)));

		$this->load->view('pagination',$data);
	}
	public function paginateAction()
	{
    	$offset = $this->uri->segment(3);

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/proizvodi/akcija';
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run("products/get_by_action"));
		$config['uri_segment'] = 3;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(3)) ? $data["offset_pg"] = $this->uri->segment(3) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);
	}	

	public function paginateFront()
	{
    	$subcat_url = $this->uri->segment(3);
		$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(2));

		//uzas jebeni
		$this->db->where("cat_id", $data['category']->id);
		$this->db->where('url',$subcat_url);
		$query = $this->db->get("subcategories");
		//kraj uzasa


		$subcategory = $query->row();

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/proizvodi/'.modules::run("categories/get_by_id",$subcategory->cat_id)->url.'/'.$subcategory->url;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run("products/get_where_custom",'subcat_id',$subcategory->id));
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginateFrontBRW()
	{
    	$id = $this->uri->segment(4);

    	$subcategory = modules::run("subcategoriesBRW/get_by_id",$id);

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/black-red-white/'.modules::run("categoriesBRW/get_by_id",$subcategory->cat_id)->url.'/'.$subcategory->url.'/'.$subcategory->id;
    	$config['per_page'] = 12;


		$config['total_rows'] = count(modules::run("productsBRW/get_where_custom",'subcat_id',$subcategory->id));
		$config['uri_segment'] = 5;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginate_search_products()
	{
    	$search = $this->uri->segment(3);


		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/proizvodi/pretraga/'.$search;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_products',$data);

	}
	public function paginate_search_categories()
	{
    	$search = $this->uri->segment(3);


		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/grupe/pretraga/'.$search;
    	$config['per_page'] = 10;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("categories/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 2;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_categories',$data);

	}
	public function paginate_search_subcategories()
	{
    	$search = $this->uri->segment(3);


		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/grupe/pretraga/'.$search;
    	$config['per_page'] = 2;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("categories/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_categories',$data);

	}
	public function paginate_search_products_admin($search)
	{
		
		//pagination---------------------------------------
		$this->load->library('pagination');

		$config['base_url'] = '/products/asearch/'.$search;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];
		$data['counter_pg'] = $data["offset_pg"] + count(modules::run("products/get_where_paginateSearch",$search,12,$this->uri->segment(4,0)));

		$this->load->view('pagination',$data);

	}				
}
