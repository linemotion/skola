$(document).ready(function() {

    $('.fancybox').fancybox();

  var animTime = 300,
  clickPolice = false;

  $(document).on('touchstart click', '.acc-btn', function(){

   if($(this).find('h3').hasClass('acc-selected'))
   {
      $(this).find('h3').removeClass("acc-selected");
      $('.acc-content').stop().animate({ height: 0 }, animTime);
   }
   else
   {

      var currIndex = $(this).index('.acc-btn'),
          targetHeight = $('.acc-content-inner').eq(currIndex).outerHeight();

      $('.acc-btn h3').removeClass('acc-selected');
      $(this).find('h3').addClass('acc-selected');

      $('.acc-content').stop().animate({ height: 0 }, animTime);
      $('.acc-content').eq(currIndex).stop().animate({ height: targetHeight }, animTime);

    }

  });




});
