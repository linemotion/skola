$(document).ready(function () {



    function createMarker(latLng, html, icon, geocoder, map, bounds) {
                var markerIcon = icon;
                var markerImage = new google.maps.MarkerImage(markerIcon,
                    new google.maps.Size(20, 32));
                var myMarker = new google.maps.Marker();
                myMarker.setPosition(latLng);
                myMarker.setMap(map);
                myMarker.setIcon(markerImage);
                var myOptions = {
                    content: html,
                    disableAutoPan: false,
                    maxWidth: 0,
                    zIndex: null,
                    boxStyle: {
                        background: "#252525",
                        opacity: 0.75,
                        width: "280px"
                    },
                    closeBoxMargin: "10px -280px 2px 2px",
                    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false,
                    pane: "floatPane",
                    enableEventPropagation: false
                };


                bounds.extend(latLng);
            }

                //Map
                // Map Init
                var mapStyle = [
                    {
                        featureType: "administrative",
                        elementType: "all",
                        stylers: [{ saturation: -20 }]
                    }, {
                        featureType: "landscape",
                        elementType: "all",
                        stylers: [{ saturation: -20 }]
                    }, {
                        featureType: "poi",
                        elementType: "all",
                        stylers: [{ saturation: -20 }]
                    }, {
                        featureType: "road",
                        elementType: "all",
                        stylers: [{ saturation: -20 }, { lightness: 13 }]
                    }, {
                        featureType: "transit",
                        elementType: "all",
                        stylers: [{ saturation: -20 }]
                    }, {
                        featureType: "water",
                        elementType: "all",
                        stylers: [{ saturation: -20 }]
                    }, {
                        featureType: "all",
                        elementType: "all",
                        stylers: []
                    }
                ];

                var baseLatLng = new google.maps.LatLng(44.815754,20.428483);
                var myOptions = {
                    zoom: 15,
                    center: baseLatLng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    mapTypeControlOptions: {
                        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'mapStyle']
                    },
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    mapTypeControl: false
                };


                var map = new google.maps.Map(document.getElementById("map"), myOptions);
                var mapCustomType = new google.maps.StyledMapType(mapStyle, myOptions);
                map.mapTypes.set('custom', mapCustomType);
                map.setMapTypeId('custom');

                var bounds = new google.maps.LatLngBounds();
                var geocoder = new google.maps.Geocoder();

                // Map Init

                // Create a marker for every address


                    var markerLatLng = new google.maps.LatLng(44.815754,20.428483);
                    var markerIcon = 'img/pin.png';
                    if (typeof markerIcon == 'undefined') {
                        markerIcon = null;
                    }
                    var markerHtml = ("<div class=\"marker-address\">" + $(this).html() + "</div>");
                    createMarker(markerLatLng, markerHtml, markerIcon, geocoder, map, bounds);

});
